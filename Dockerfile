FROM node:14.16.0-alpine3.13 AS development
WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV=${NODE_ENV}

COPY package*.json ./
RUN npm install

COPY . .
RUN npm run build
CMD node dist/main
